#include "pch.h"
#include <Windows.h>
#include <iostream>

using namespace std;

BOOL is_exit_from_async_infinity_loop_permitted = FALSE;

HANDLE open_pipe();
std::string read_file_from_pipe(HANDLE pipe);
VOID CALLBACK on_io_complete(DWORD, DWORD, OVERLAPPED);

int main()
{
    cout << "Hello World! I am a client." << endl; 
	// Ждем пока запустится сервер (В дальнейшем идет проверка, так что это не обязательно)
	// Sleep(300);

	HANDLE nice_pipe = open_pipe();
	string message;
	while (message != "exit"){
		message = read_file_from_pipe(nice_pipe);
	}
	CloseHandle(nice_pipe);

	system("pause");
	return 0;
}

HANDLE open_pipe()
{
	LPCSTR pipe_name = "\\\\.\\pipe\\this_is_not_a_pipe";

	HANDLE pipe;
	while (TRUE) {
		pipe = CreateFileA(
			pipe_name,
			GENERIC_ALL,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_FLAG_OVERLAPPED,
			NULL
		);

		if (pipe != INVALID_HANDLE_VALUE)
		{
			cout << "Connection with server established." << endl;
			break;
		}
		else 
		{
			cout << "Could not open pipe. Waiting..." << endl;
			Sleep(30);
		}
	}

	return pipe;
}

std::string read_file_from_pipe(HANDLE pipe)
{
	char recieved_message[512] = {};
	while (TRUE) {
		OVERLAPPED overlapped_struct{};
		ReadFileEx(
			pipe,
			recieved_message,
			512,
			&overlapped_struct,
			(LPOVERLAPPED_COMPLETION_ROUTINE) on_io_complete
		);
		SleepEx(INFINITE, 1);

		if (is_exit_from_async_infinity_loop_permitted)
		{
			break;
		}
	}
	cout << "Message recieved from sever: " << recieved_message << endl;
	return recieved_message;
}

VOID CALLBACK on_io_complete(DWORD, DWORD, OVERLAPPED)
{
	is_exit_from_async_infinity_loop_permitted = TRUE;
}
