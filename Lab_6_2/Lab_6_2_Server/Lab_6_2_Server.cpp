#include "pch.h"
#include <Windows.h>
#include <iostream>
#include <string>

using namespace std;

OVERLAPPED overlapped_struct{};

HANDLE create_pipe();
void connect_pipe(HANDLE pipe);
HANDLE create_event();
void write_message_to_pipe(HANDLE pipe, string message);
void wait_for_event(HANDLE event);
void disconnect_pipe(HANDLE pipe);

int main()
{
	cout << "Hello World! I am a server." << endl;

	HANDLE some_event = create_event();
	overlapped_struct.hEvent = some_event;

	HANDLE nice_pipe = create_pipe();
	cout << "Pipe was created." << endl;

	connect_pipe(nice_pipe);
	cout << "Pipe connection was established." << endl;
	
	string message_to_send{};
	while (message_to_send != "exit") {
		cout << "Type in message, that you want to send to client (type in \"exit\" to stop): ";
		getline(cin, message_to_send);
		write_message_to_pipe(nice_pipe, message_to_send);
		wait_for_event(some_event);
	}
	disconnect_pipe(nice_pipe);

	CloseHandle(nice_pipe);
	CloseHandle(some_event);

	system("pause");
	return 0; 
}

HANDLE create_pipe()
{
	LPCSTR pipe_name = "\\\\.\\pipe\\this_is_not_a_pipe";
	DWORD open_mode = PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED;
	DWORD pipe_mode = PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT;
	DWORD max_instances = PIPE_UNLIMITED_INSTANCES;
	DWORD buffer_size = 512;
	DWORD default_timeout = 0;
	LPSECURITY_ATTRIBUTES security_attributes = NULL;

	HANDLE pipe = CreateNamedPipeA(
		pipe_name,
		open_mode,
		pipe_mode,
		max_instances,
		buffer_size,
		buffer_size,
		default_timeout,
		security_attributes
	);

	if (pipe == INVALID_HANDLE_VALUE) {
		cerr << "Pipe was not created. Error code: " 
			<< GetLastError() 
			<< ". Sorry, goodbye."
			<< endl;
		exit(-1);
	}

	return pipe;
}

void connect_pipe(HANDLE pipe)
{
	BOOL is_pipe_connected = ConnectNamedPipe(
		pipe,
		NULL
		// &overlapped_struct
	); 

	if (!is_pipe_connected) {
		cerr << "Pipe was not connected. Error code: " 
			<< GetLastError() 
			<< ". Sorry, goodbye."
			<< endl;
		exit(-1);
	}
}

HANDLE create_event()
{
	LPSECURITY_ATTRIBUTES event_attributes = NULL;
	BOOL manual_reset = FALSE;
	BOOL initial_state = FALSE;
	LPCSTR name = "I predict some event will happen!";

	HANDLE some_event = CreateEventA(
		event_attributes,
		manual_reset,
		initial_state,
		name
	);

	return some_event;
}

void write_message_to_pipe(HANDLE pipe, string message)
{
	const char * message_cstr = message.c_str();
	WriteFile(
		pipe,
		message_cstr,
		512,
		NULL,
		&overlapped_struct
	);
}

void wait_for_event(HANDLE event)
{
	DWORD cause_of_resuming = WaitForSingleObject(event, INFINITE);

	if (cause_of_resuming == WAIT_OBJECT_0) 
	{
		cout << "Event \"" << event << "\" just happened, thus message was sent!" << endl;
	} else {
		cout << "Execution was resumed for some other reason." << endl;
	}
}

void disconnect_pipe(HANDLE pipe)
{
	BOOL is_pipe_disconnected = DisconnectNamedPipe(pipe);

	if (!is_pipe_disconnected)
	{
		cout << "Disconnection error. Error code: " << GetLastError() << "." << endl;
	}
}

