﻿#include "pch.h"
#include <windows.h>
#include <iomanip>
#include <fstream>
#include <time.h>
#include <vector>
#include <string>
#include <stdlib.h>
#include <chrono>
#include <random>
#include <bitset>

using namespace std;

vector<string> create_page_mutexes_names();
void open_log(fstream& stream);
PVOID get_mapped_file();
void lock_memory(char* mapped_file_in_memory);
int get_page_for_reading(char* mapped_file_in_memory);
void try_read_page(char* mapped_file_in_memory, int page, fstream & log_file);

void log_waiting_for_a_file(fstream& log);
void log_ready_for_reading(fstream& log);
void log_start_reading(fstream& log);
void log_readed(fstream& log, int page, char* data);
void log_page_is_empty(fstream& log, int page);

long long get_now();

const int NUMBER_OF_MEMORY_PAGES = 16;
const int PAGE_SIZE = 4096;
const int NUMBER_OF_TRIALS = 10;

DWORD process_pid = GetCurrentProcessId();
bitset<NUMBER_OF_MEMORY_PAGES> readed_pages{};

int main()
{
	vector<string> mutexes_names = create_page_mutexes_names();
	fstream log_file;
	open_log(log_file);
	char* mapped_file_in_memory = (char*) get_mapped_file();
	lock_memory(mapped_file_in_memory);

	HANDLE reader_semaphore = OpenSemaphoreA(SEMAPHORE_ALL_ACCESS, FALSE, "reader_semaphore");

	while (!readed_pages.all()) {
		log_waiting_for_a_file(log_file);

		WaitForSingleObject(reader_semaphore, INFINITE);
		
		log_ready_for_reading(log_file);

		int page = get_page_for_reading(mapped_file_in_memory);

		HANDLE mutex = OpenMutexA(MUTEX_ALL_ACCESS, TRUE, mutexes_names[page].c_str());

		WaitForSingleObject(mutex, INFINITE);

		log_start_reading(log_file);

		try_read_page(mapped_file_in_memory, page, log_file);

		ReleaseMutex(mutex); 

		ReleaseSemaphore(reader_semaphore, 1, NULL); 
	}

	log_file.close();

	return 0;
}

vector<string> create_page_mutexes_names()
{
	vector<string> mutexes_names{};
	mutexes_names.reserve(NUMBER_OF_MEMORY_PAGES);
	for (int i = 0; i < NUMBER_OF_MEMORY_PAGES; i++) {
		mutexes_names.emplace_back(string{"page_mutex_" + to_string(i)});
	}
	return mutexes_names;
}

void open_log(fstream& stream)
{
	string log_path = "logs\\logReader.txt";
	stream.open(log_path, fstream::out | fstream::app);
}

PVOID get_mapped_file()
{
	HANDLE hMemory = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, "file");
	return MapViewOfFile(hMemory, FILE_MAP_ALL_ACCESS, 0, 0, 0);
}

void lock_memory(char* mapped_file_in_memory)
{
	VirtualLock((PVOID) mapped_file_in_memory, NUMBER_OF_MEMORY_PAGES * PAGE_SIZE + NUMBER_OF_MEMORY_PAGES * sizeof(char));
}

int get_page_for_reading(char * mapped_file_in_memory)
{
	mt19937 rng;
	rng.seed(random_device()());
	uniform_int_distribution<mt19937::result_type> dist(0, NUMBER_OF_MEMORY_PAGES - 1);

	for (int j = dist(rng); !readed_pages.all(); j = (j + 1) % NUMBER_OF_MEMORY_PAGES) {
		bitset<NUMBER_OF_MEMORY_PAGES> page_mask{(unsigned __int64)1 << j};
		if ((readed_pages & page_mask) == 0) {
			return j;
		}
	}
}

void try_read_page(char* mapped_file_in_memory, int page, fstream & log_file)
{
	mt19937 rng;
	rng.seed(random_device()());
	uniform_int_distribution<mt19937::result_type> dist(0, 1000);

	if (mapped_file_in_memory[page]) {
		char data[PAGE_SIZE];
		int offset = page * PAGE_SIZE + NUMBER_OF_MEMORY_PAGES * sizeof(char);
		memcpy(data, mapped_file_in_memory + offset, PAGE_SIZE);
		Sleep(500 + dist(rng));

		readed_pages |= (1 << page);

		log_readed(log_file, page, data);
	} else {
		log_page_is_empty(log_file, page);
		Sleep(500);
	}
}

void log_waiting_for_a_file(fstream & log)
{
	log << "[" << get_now() << "] (Reader PID: " << setw(4) << process_pid << ") Waiting..." << endl;
}

void log_ready_for_reading(fstream & log)
{
	log << "[" << get_now() << "] (Reader PID: " << setw(4) << process_pid << ") Ready to read!" << endl;
}

void log_start_reading(fstream & log)
{
	log << "[" << get_now() << "] (Reader PID: " << setw(4) << process_pid << ") Started reading!" << endl;
}

void log_readed(fstream & log, int page, char * data)
{
	log << "[" << get_now() << "] (Reader PID: " << setw(4) << process_pid << ") Readed from page " << setw(2) << page << ". Data is: \"" << data << "\"" << endl;
}

void log_page_is_empty(fstream & log, int page)
{
	log << "[" << get_now() << "] (Reader PID: " << setw(4) << process_pid << ") Failed. Page is empty. Page number: " << setw(2) << page << "." << endl;
}

long long get_now()
{
	chrono::steady_clock::time_point now = chrono::high_resolution_clock::now();
	return chrono::duration_cast<chrono::nanoseconds>(now.time_since_epoch()).count();
}

