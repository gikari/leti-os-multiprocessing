#include "pch.h"
#include <windows.h>
#include <fstream>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <bitset>
#include <vector>
#include <string>
#include <stdlib.h>
#include <random>

using namespace std;
const int NUMBER_OF_MEMORY_PAGES = 16;
const int PAGE_SIZE = 4096;
DWORD process_pid = GetCurrentProcessId();
vector<string> create_page_mutexes_names();

int
long long get_now();

int main() {
    mt19937 rng;
    rng.seed(random_device()());
    uniform_int_distribution<mt19937::result_type> dist(0, 1000);
    uniform_int_distribution<mt19937::result_type> num_page(0, NUMBER_OF_MEMORY_PAGES-1);
    vector<string> mutexes_names = create_page_mutexes_names();
    bitset<NUMBER_OF_MEMORY_PAGES> writer_pages{};
    fstream log_file;
    string log_path = "logs\\logWriter.txt";
    log_file.open(log_path, fstream::out | fstream::app);

    HANDLE hMemory = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, "file");
    char *memory = (char *) MapViewOfFile(hMemory, FILE_MAP_ALL_ACCESS, 0, 0, 0);

    VirtualLock((PVOID) memory, NUMBER_OF_MEMORY_PAGES * PAGE_SIZE + NUMBER_OF_MEMORY_PAGES * sizeof(char));

    HANDLE writer_semaphore = OpenSemaphoreA(SEMAPHORE_ALL_ACCESS, FALSE, "writer_semaphore");
	//получаем рандомную сраницу с которой начинаем писать ( все страницы точно напишуться так как мы не закончим пока все не будут записаны)
	// и делаем это по кругу. Это увеличи разброс и поулучается не все пишут в 0 страницу и ждут пока освободится
	// по сути тоже самое но старт с рандом страницы что поможет не ждать
    for (int page = num_page(rng); !writer_pages.all(); page = ( page + 1 ) % NUMBER_OF_MEMORY_PAGES ) {
        writer_pages |= (1 << page);
        log_file << "[" << get_now() << "] (Writer PID: " << setw(4) << process_pid << ") Waiting..." << endl;

        WaitForSingleObject(writer_semaphore, INFINITE);

        log_file << "[" << get_now() << "] (Writer PID: " << setw(4) << process_pid << ") Ready to write!" << endl;
		
		char data[PAGE_SIZE];
        sprintf_s(data, "Page number: %2d. Writer PID: %4d.", page, process_pid);
        HANDLE mutex = OpenMutexA(MUTEX_ALL_ACCESS, TRUE, mutexes_names[page].c_str());
        WaitForSingleObject(mutex, INFINITE);
        log_file << "[" << get_now() << "] (Writer PID: " << setw(4) << process_pid << ") Started writing!" << endl;
        int offset = page * PAGE_SIZE + NUMBER_OF_MEMORY_PAGES * sizeof(char);
        memcpy(memory + offset, data, PAGE_SIZE);
        Sleep(500 + dist(rng));

        memory[page] = 1;
        log_file << "[" << get_now() << "] (Writer PID: " << setw(4) << process_pid << ") Data was written into page "
                 << setw(2) << page << endl;
        ReleaseMutex(mutex);
        ReleaseSemaphore(writer_semaphore, 1, NULL);
    }

    return 0;
}

vector<string> create_page_mutexes_names() {
    vector<string> mutexes_names{};
    mutexes_names.reserve(NUMBER_OF_MEMORY_PAGES);
    for (int i = 0; i < NUMBER_OF_MEMORY_PAGES; i++) {
        mutexes_names.emplace_back(string{"page_mutex_" + to_string(i)});
    }
    return mutexes_names;
}

long long get_now() {
    chrono::steady_clock::time_point now = chrono::high_resolution_clock::now();
    return chrono::duration_cast<chrono::nanoseconds>(now.time_since_epoch()).count();
}

