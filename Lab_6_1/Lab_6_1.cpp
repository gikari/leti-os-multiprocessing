#include "pch.h"
#include <windows.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <cmath>

using namespace std;

const int READERS_AMOUNT = 16;
const int WRITERS_AMOUNT = 16;

const int SEMAPHORE_MAX_VALUE = 16;
const int NUMBER_OF_MEMORY_PAGES = 16;
const int MEMORY_PAGE_SIZE = 4096;
const int PROCESS_WAITING_TIMEOUT_MS = INFINITE;//1.5 * NUMBER_OF_MEMORY_PAGES * 1000;

const LPCSTR PATH_TO_WRITER = "Debug/Lab_6_1_Writer.exe";
const LPCSTR PATH_TO_READER = "Debug/Lab_6_1_Reader.exe";

PROCESS_INFORMATION writer_process_info[WRITERS_AMOUNT]{};
PROCESS_INFORMATION reader_process_info[READERS_AMOUNT]{};

int writers_count = 0;
int readers_count = 0;

void clean_logs();
void create_semaphores();
vector<string> create_page_mutexes_names();
void create_page_mutexes();
void create_clean_mapped_file();
void create_reader(int process_number);
void create_writer(int process_number);
void create_all_processes();
void wait_for_processes();

int main()
{
	clean_logs();
	create_semaphores();
	create_page_mutexes();

	create_clean_mapped_file();
	create_all_processes();
	wait_for_processes();

	system("pause");
	return 0;
}

void clean_logs()
{
	fstream log_file;
	string writer_log_path = "logs\\logWriter.txt";
	log_file.open(writer_log_path, fstream::out | fstream::trunc);
	log_file << "========= START =========" << endl;
	log_file.close();

	string reader_log_path = "logs\\logReader.txt";
	log_file.open(reader_log_path, fstream::out | fstream::trunc);
	log_file << "========= START =========" << endl;
	log_file.close();
}

void create_semaphores()
{
	CreateSemaphoreA(NULL, SEMAPHORE_MAX_VALUE, SEMAPHORE_MAX_VALUE, "writer_semaphore");
	CreateSemaphoreA(NULL, SEMAPHORE_MAX_VALUE, SEMAPHORE_MAX_VALUE, "reader_semaphore");
}

vector<string> create_page_mutexes_names()
{
	vector<string> mutexes_names{};
	mutexes_names.reserve(NUMBER_OF_MEMORY_PAGES);
	for (int i = 0; i < NUMBER_OF_MEMORY_PAGES; i++) {
		mutexes_names.emplace_back(string{"page_mutex_" + to_string(i)});
	}
	return mutexes_names;
}

void create_page_mutexes()
{
	vector<string> mutexes_names = create_page_mutexes_names();
	for (int i = 0; i < NUMBER_OF_MEMORY_PAGES; i++) {
		CreateMutexA(NULL, FALSE, mutexes_names[i].c_str());
	}
}

void create_clean_mapped_file()
{
	HANDLE file = CreateFileA(
		"file.txt",
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		CREATE_NEW,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);

	DWORD file_size = NUMBER_OF_MEMORY_PAGES * MEMORY_PAGE_SIZE + NUMBER_OF_MEMORY_PAGES * sizeof(char);

	HANDLE hMemory = CreateFileMappingA(
		file,
		NULL,
		PAGE_READWRITE,
		0,
		file_size,
		"file"
	);

	char* memory = (char*) MapViewOfFile(hMemory, FILE_MAP_ALL_ACCESS, 0, 0, 0);
	ZeroMemory(memory, file_size);
}

void create_reader(int process_number)
{
	STARTUPINFOA startinfo {};
	if (!CreateProcessA(PATH_TO_WRITER, NULL, NULL, NULL, TRUE, 0, NULL, NULL, &startinfo, &(writer_process_info[writers_count++])))
		cout << "Manager: couldn't create writer process #" << process_number << " error code: " << GetLastError() << endl;
	else
		cout << "\rCreated Reader #" << writers_count << flush;

}

void create_writer(int process_number)
{
	STARTUPINFOA startinfo {};
	if (!CreateProcessA(PATH_TO_READER, NULL, NULL, NULL, TRUE, 0, NULL, NULL, &startinfo, &(reader_process_info[readers_count++]))) {
		cout << "Manager: couldn't create reader process #" << process_number << " error code: " << GetLastError() << endl;
	} else {
		cout << "\rCreated Writer #" << readers_count << flush;
	}
}

void create_all_processes()
{
	cout << "Creating processes..." << endl;
	for (int process_number = 0; process_number < min(READERS_AMOUNT, WRITERS_AMOUNT); process_number++) {
		create_writer(process_number);
		create_reader(process_number);
	}

	int remaining_proceses_amount = abs(READERS_AMOUNT - WRITERS_AMOUNT);

	for (int process_number = 0; process_number < remaining_proceses_amount; process_number++) {
		int process_number_in_name = process_number + min(READERS_AMOUNT, WRITERS_AMOUNT);
		READERS_AMOUNT > WRITERS_AMOUNT ? create_reader(process_number_in_name) : create_writer(process_number_in_name);
	}

	cout << "\rCreated all processes!" << endl;
}

void wait_for_processes() 
{
	for (int i = 0; i < WRITERS_AMOUNT; i++) {
		Sleep(500);
		cout << "\rWaiting for Writer #" << i << flush;
		DWORD exit_code = WaitForSingleObject(writer_process_info[i].hProcess, PROCESS_WAITING_TIMEOUT_MS);

		if (exit_code == WAIT_TIMEOUT) {
			cout << " (Timeout); " << endl;
		} 
	}

	cout << "\r                                       " << flush;

	for (int i = 0; i < READERS_AMOUNT; i++) {
		Sleep(500);
		cout << "\rWaiting for Reader #" << i << flush;
		DWORD exit_code = WaitForSingleObject(reader_process_info[i].hProcess, PROCESS_WAITING_TIMEOUT_MS);

		if (exit_code == WAIT_TIMEOUT) {
			cout << " (Timeout); " << endl;
		} 
	}
	cout << "\rWaiting complete.                         " << endl;
}

